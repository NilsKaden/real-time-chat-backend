FROM golang
ADD . app/
RUN go get -d -v ./...
RUN go build -v ./...
WORKDIR app/src/hello
EXPOSE 8000
CMD go run hello.go